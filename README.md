# Building a jenkins pipeline for a java-maven application.
- ## note: check out the other branches for other pipeline jobs

## Jenkinfile pipeline syntax

- scripted pipeline
```
node {
    //groovy script
    //this automatically declares a pipeline and the agent type compared to the declarative pipeline.
}
```

- declarative pipeline
```
pipeline { //top level command

    agent any //where to run the jenkinsfile (which node)
    
    stages { //where the work starts
        
        stage("build") {
            
            steps{
            }
        }
    }
}
```

## Jenkinsfile attributes

### - post attribute
```
post { //this executes some logic after all the stages are executed
    //conditions
    always{
        //the script within tis block gets executed always, regardless of build success or failure
    }

    success {
        //the script within tis block gets executed if and only if the build was successful 
    }

    failure {
        //the script within tis block gets executed if and only if the build fails
    }

} 
```

### - tools attribute
this make build tools available for the application (supported for: gradle, maven, jdk)

```
pipeline {
    agent any 
    tools {
        tool toolName //the toolName is configured in the build section in  jenkins 
        maven 'Maven'
    }

    stages {
        stage("build") {
            steps {
                //it provides access to mvn commands
                sh "mvn package"
            }
        }
    }
}
```

### - parameters attribute

```
pipeline {
    agent any 
    parameters {
        //parameterType(name: "", defaultValue: "", description: "")
        string(name: "VERSION", defaultValue: "", description: "version to build")
        choice(name: "VERSION", choices: ["1.0","1.1","1.2"], description: ")
        booleanParam(name: "executeBuild", defaultValue: true, description: "")
    }

    stages {
        stage("build") {
            steps {
                //they are suitable for when expressions
                when {
                    expression {
                        params.executeBuild
                    }
                }

                echo "building version ${VERSION}"
            }
        }
    }
}
```


### - Defining conditionals or expressions for each stage.
```
CODE_CHANGES = getGitChanges()
...
  
stage("build") {
    when {
        expression {
            BRANCH_NAME == 'dev' || BRANCH_NAME == 'master' 
            CODE_CHANGES == true

        }
    }
}
```

### - Environmental variables in jenkinsfile
The in-built jenkins environmental variables are all available in  'jenkinsUrl/env-vars.html'. In addition, we can create environmental variables using the 'enironment' block

```
pipeline {
    agent any
    environment {
        NEW_VERSION = '1.3.0'
    }
    stages {
        stage ("build") {
            steps {
                //to reference the defined environmental variable 'NEW_VERSION'
                echo "building the version ${NEW_VERSION}" //must be in double quotes
            }
        }
    }
}
```
### - Using credentials in Jenkinsfile
this can be done in two ways, 

using the environment block
```
pipeline {
    agent any
    environment {
        SERVER_CREDENTIALS = credentials('credentialId')
    }

    stages {
        stage("build") {
            steps {
                //to reference the defined credential
                sh "... login ${SERVER_CREDENTIALS}"
            }
        }
    }
}
```

using the wrapper syntax
```
pipeline{
    agent any

    stages {
        stage('build') {
            steps {
                echo "building the application..."
                withCredentials([ //this takes an object
                    usernamePassword(credentials: 'credentialId', usernameVariable: USER, passwordVariable: PWD) {
                        //to reference variables
                        sh "some script ${USER} ${PWD}"
                    }
                ])
            }
        }
    }
}
```


### - User inputs in Jenkinsfile, there are two methods:

- method1
```
pipeline {
    agent any 

    stages {
        stage ("deploy") {
            //to take user input
            input {
                message "select the environment to deploy to"
                ok "done"
                parameters {
                    choice(name: "ENV", choices: ["development", "staging", "production"], description: "")
                }
            }

            steps {
                echo "deploying to ${ENV} environment..."
            }
        }
    }
}
```

- method2
```
pipeline {
    agent any

    stages {
        stage ("deploy") {
            step {
                script {
                    env.ENV = input message: "select the environment to deploy to", ok: "done", parameters: [choice(name: "ONE", choices: ["dev", "prod"], description: "")] 
                    echo "Deploying to ${ENV} enironment..."
                }
            }
        }
    }
}
```

## Using external groovy scripts in Jenkinsfile

create a jenkinsfilke and insert the code below
```
def gv //this defines a variable 'gv' that will be used to load scripts.
pipeline {
    agent any
    
    stages {
        stage ("init") {
            steps {
                script {
                    gv = load "script.groovy"
                }
            }
        }
        stage ("build") {
            steps {
                script {
                    //variableName.functionName()
                    gv.buildApp()
                }
            }
        }
    }
}
```

create a script.groovy file and insert the code below
```
def buildApp() {
    echo "building the application..."
}

return this
```
